#include<iostream>

using namespace std;

string process_query(){
	int number_of_stones;
	cin>>number_of_stones;
	int k=0;
	if(number_of_stones%2==1){
		++number_of_stones;
		++k;
	}
	int x;
	int previous_value = 0;
	int result = 0;
	while(k<number_of_stones){
		cin>>x;
		if(k%2==1)result ^= (x-previous_value);
		previous_value = x;
		++k;
	}

	return result == 0 ? "NIE" : "TAK";
}


int main(){
	int number_of_queries;
	cin>>number_of_queries;
	while(number_of_queries>0){
		cout<<process_query()<<endl;
		--number_of_queries;
	}

	return 0;
}
